# #####################################################################
# Copyright (c) 2017, NMSU Board of Regents
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# #####################################################################

# Standard library imports.
import argparse
import datetime
import math
import sys

# NMCC-Calc-Ra package imports.
from cc.calc.ra import daily as ra


def _Rs_div_Rso(Rs, Rso):

    rs_div_rso = Rs / Rso

    if rs_div_rso < 0.3:
        rs_div_rso = 0.3

    elif rs_div_rso > 1.0:
        rs_div_rso = 1

    return rs_div_rso


def eth(doy, lat, Tx, Tn, Ra = None):

    try:

        # Step 1.  Extraterrestrial radiation (Ra) is calculated for the
        # day of using equations from Duffie and Beckman (1980).
        if Ra is None:
            Ra = ra(doy, lat)

        # Step 2.  Calculate ETh using the Hargreaves-Samani (1982)
        # equation.
        s2_term1 = 0.0023 * Ra
        s2_term2 = ((Tx + Tn) / 2.0) + 17.8
        s2_term3 = math.sqrt(Tx - Tn)

        ETh = 0.408 * (s2_term1 * s2_term2 * s2_term3)

    except:
        ETh = float('nan')

    return ETh


def etor(doy, lat, ele, Tx, Tn, RHx, RHn, WSa, Rs, Ra = None):

    try:

        # Step 1.  Extraterrestrial radiation (Ra) is calculated for the
        # day of using equations from Duffie and Beckman (1980).
        if Ra is None:
            Ra = ra(doy, lat)

        # STEP 2.  Calculate the daily net radiation (Rn) expected over
        # grass in MJ/m^2/d using equations from Allen et al.(1994).
        sigma = 0.00000000490

        Rso = Ra * (0.75 + 0.00002 * ele)
        Rns = (1 - 0.23) * Rs
        f = (1.35 * (_Rs_div_Rso(Rs, Rso))) - 0.35

        e_sub_s = lambda T: 0.6108 * math.exp((17.27 * T) / (T + 237.3))
        esTx = e_sub_s(Tx)
        esTn = e_sub_s(Tn)

        ed = (esTx * (RHn / 100.0) + esTn * (RHx / 100.0)) / 2.0
        epsilon_prime = 0.34 - 0.14 * math.sqrt(ed)

        lf = lambda T: math.pow(T + 273.15, 4)
        s2_term1 = -1.0 * f * epsilon_prime * sigma
        s2_term2 = (lf(Tx) + lf(Tn)) / 2.0
        Rnl = s2_term1 * s2_term2

        Rn = Rns + Rnl

        # Step 3.  Calculate variables needed to compute ETo and ETr.
        s3_term = math.pow((293 - 0.0065 * ele) / 293.0, 5.26)
        beta = 101.3 * s3_term
        gamma = 0.00163 * (beta / 2.45)
        Tm = (Tx + Tn) / 2.0
        e_super_o = e_sub_s(Tm)
        Delta = (4099 * e_super_o) / math.pow(Tm + 237.3, 2)
        ea = (esTx + esTn) / 2.0

        # Step 4.  Calculate ETo using the ASCE-EWRI (2004) standardized
        # equation for short canopy reference ET.
        G = 0.0

        s4_term1 = 0.408 * Delta * (Rn - G)
        s4_term2 = Delta + gamma * (1 + 0.34 * WSa)
        Ro = s4_term1 / s4_term2

        s4_term3 = (900 * gamma) / (Tm + 273)
        s4_term4 = (ea - ed)
        Ao = (s4_term3 * WSa * s4_term4) / s4_term2

        ETo = Ro + Ao

        # Step 5.  Calculate ETr using the ASCE-EWRI (2004) standardized
        # equation for tall canopy reference ET.
        s5_term1 = 0.408 * Delta * (Rn - G)
        s5_term2 = Delta + gamma * (1 + 0.38 * WSa)
        Rr = s5_term1 / s5_term2

        s5_term3 = (1600 * gamma) / (Tm + 273)
        s5_term4 = (ea - ed)
        Ar = (s5_term3 * WSa * s5_term4) / s5_term2

        ETr = Rr + Ar

    except Exception as e:

        ETo = float('nan')
        ETr = float('nan')

    return (ETo, ETr)
